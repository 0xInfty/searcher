const { ObjectID } = require("bson");
const axios = require("axios");

const urlLocations = 'http://' + process.env.LOCATIONS_SERVICE + ':' + process.env.LOCATIONS_PORT;
const urlIndividuals = 'http://' + process.env.INDIVIDUALS_SERVICE + ':' + process.env.INDIVIDUALS_PORT;
const urlObjects = 'http://' + process.env.OBJECTS_SERVICE + ':' + process.env.OBJECTS_PORT;
const urlSocieties = 'http://' + process.env.SOCIETIES_SERVICE + ':' + process.env.SOCIETIES_PORT;
const urlEvents = 'http://' + process.env.EVENTS_SERVICE + ':' + process.env.EVENTS_PORT;

const verbosityLevel = 1;
const consoleLogDebug = (stringToConsoleLog, importanceLevel=0) => {
    if ( importanceLevel <= verbosityLevel ) { console.log( stringToConsoleLog ) }
}

async function search( searchString, filters ) { return new Promise(async (resolve, reject) => { 

    const objectsFound = [];
    for (const filter of filters) {
        if( filter.check ) {
            const url = getUrl(filter.filter) 
            await axios.post( url + "/search", {searchString}).then( response => {
                const objects = response.data
                if(objects.length !== 0) {
                    for (const object of objects) {
                        objectsFound.push( object )
                    }
                }
                consoleLogDebug(objectsFound, 2)
            })
        }
    };
    resolve (objectsFound)

})}

async function searchRecent( maxNumber, filters ) { return new Promise(async (resolve, reject) => { 

    let objectsFound = [];
    for (const filter of filters) {
        if( filter.check ) {
            const url = getUrl(filter.filter) 
            await axios.post( url + "/recent", {maxNumber}).then( response => {
                const objects = response.data
                if(objects.length !== 0) {
                    for (const object of objects) {
                        objectsFound.push( object )
                    }
                }
                consoleLogDebug(objectsFound, 2)
            })
        }
    };
    objectsFound.sort((a, b) => b.created_ts - a.created_ts)
    objectsFound = objectsFound.slice(0, maxNumber)
    resolve (objectsFound)

})}

function getUrl(serviceName){
    switch(serviceName){
        case 'locations': return ( urlLocations + '/locations'); 
        case 'objects': return (urlObjects + '/objects');
        case 'individuals': return (urlIndividuals + '/individuals'); 
        case 'events': return (urlEvents + '/events'); 
        case 'societies': return (urlSocieties + '/societies'); 
        default: return "error";
    }
}

module.exports = { search, searchRecent }