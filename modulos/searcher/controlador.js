const service = require('./servicio')
const {logger} = require('../../logger')

const default_filters = [
    { filter:"events", check:true },
    { filter:"locations", check:true },
    { filter:"individuals", check:true },
    { filter:"societies", check:true },
    { filter:"objects", check:true },
]
const default_maxNumber = 20

async function search(req, res){
    try{
        console.log(req.body)
        const searchString = req.body.searchString;
        const filters = req.body.filters;
        logger.debug({ action: 'Request to search, string: ' +  searchString})
        const objectsFound = await service.search( searchString, filters)
        res.writeHead(201, 'Content-Type', 'application/json')
        res.end(JSON.stringify(objectsFound))
    } catch {
        logger.info({ action: 'Fail in search' })
        res.writeHead(500, 'Content-Type', 'application/json')
        res.end(JSON.stringify({ result:"Fail en search"}))
    }
}

async function searchRecent(req, res){
    try{
        console.log(req.body)
        var filters
        if (req.body.filters === undefined) {
            filters = default_filters;
        } else {
            filters = req.body.filters;
        }
        var maxNumber
        if (req.body.maxNumber === undefined) {
            maxNumber = default_maxNumber;
        } else {
            maxNumber = req.body.maxNumber;
        }
        logger.debug({ action: `Request to search the most recent entries using filters ${filters} and maxNumber ${maxNumber}`})
        const objectsFound = await service.searchRecent( maxNumber, filters )
        res.writeHead(201, 'Content-Type', 'application/json')
        res.end(JSON.stringify(objectsFound))
    } catch {
        logger.info({ action: 'Fail in search' })
        res.writeHead(500, 'Content-Type', 'application/json')
        res.end(JSON.stringify({ result:"Fail en search"}))
    }
}

module.exports = { search, searchRecent }